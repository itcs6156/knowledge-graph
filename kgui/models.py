from djongo import models

class Politician(models.Model):
    name = models.TextField(unique=True)
    image = models.TextField()
    content = models.TextField()
    reference = models.ListField()
