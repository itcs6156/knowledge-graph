from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render

from .models import Politician
from lib.extract_concepts import ExtractConcepts
from lib.extract_relations import ExtractRelations


def index(request):
    politician_list = Politician.objects.order_by('name').all()
    paginator = Paginator(politician_list, 60)
    
    page = request.GET.get('page')
    politicians = paginator.get_page(page)
    return render(request, 'index.html', {'politicians': politicians})


def detail(request, politician_id):
    politician = Politician.objects.all().filter(id=politician_id)
    if len(politician) > 0:
        extract_concepts = ExtractConcepts()
        politician = politician[0]
        concepts = extract_concepts.extract(politician.content)
        concepts = [' '.join([c[0] for c in concept]) for concept in concepts]
        
        output = ExtractRelations.extract_relations(politician.content)
        relations = []
        
        if 'sentences' in output:
            for s in output['sentences']:
                if 'openie' in s:
                    for r in s['openie']:
                        relations.append(r['subject'] + ' {' + r['relation'] + '} ' + r['object'])
        
    return render(request, 'detail.html', {'relations': relations})
