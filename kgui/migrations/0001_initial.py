# Generated by Django 2.0.3 on 2018-03-31 15:17

from django.db import migrations, models
import djongo.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Politician',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(unique=True)),
                ('image', models.TextField()),
                ('content', models.TextField()),
                ('reference', djongo.models.fields.ListField()),
            ],
        ),
    ]
