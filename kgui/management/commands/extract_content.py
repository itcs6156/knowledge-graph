import signal
import datefinder
import regex

from os import listdir
from os.path import isfile, join
from django.core.management.base import BaseCommand
from kgui.models import Politician
from django.conf import settings


class Command(BaseCommand):
    help = 'Extract the politiciants content from the files and upload them to the server'
    
    def __init__(self):
        super().__init__()
        
        self.index = 0
        #signal.signal(signal.SIGSEGV, self.sig_handler)
    
    def extract(self, path, file):
        content = open(join(path, file), 'r').read()
        
        page = Politician()
        
        # Extract Name
        match = regex.search(r'\'\'\'(.*?)\'\'\'', content)
        if match is not None:
            page.name = match.group(1).strip()
        else:
            page.name = file.replace('_', ' ')
        
        # Extract image file name
        match = regex.search(r'[=|:](.*\.jpg)', content)
        if match is not None:
            page.image = match.group(1).strip()
        
        # Extract URL references
        match = regex.findall(r'(https?://[^\s\]\}<]+)', content)
        if match is not None:
            page.reference = list(set(match))
        else:
            page.reference = list()
        
        # Remove infobox
        content = regex.sub(r'{((?>[^{}]+|(?R))*)}', '', content, regex.MULTILINE)
        
        # Remove other braces {{...}}
        content = regex.sub(r'{{[^{}]+?}}', '', content)
        
        # Remove all references <ref..>
        content = regex.sub(r'<[^>]+?>', '', content)
        
        # Remove all categories [[Category:...]]
        content = regex.sub(r'\[\[Category[^]]+\]\]?', '', content)
        
        # Remove inline images
        content = regex.sub(r'\[\[File[^\]]+\]\]?', '', content)
        
        # Remove square brackets with pipes and pick second option e.g. [[a|b]] replaces with b
        content = regex.sub(r'\[\[([^\|\[]+)\|([^\]]+)\]\]?', r'\2', content)
        
        # Remove remaining square brackets [[]]
        content = regex.sub(r'\[\[([^\]]+)\]\]?', r'\1', content)
        
        # Remove links and text in the square bracket
        content = regex.sub(r'\[http[^\s]+ ([^\]]+)]?', r'\1', content)
        
        # Indicate born date
        content = content.replace('\'\'\' (', '\'\'\' (born ')
        
        # Remove ''' from name
        content = content.replace("'''", '')
        
        # Remove == from section title
        content = content.replace('==', '')
        
        # Remove white spaces
        content = content.strip()
        
        # Convert date to YYYYMMDD formats
        matches = datefinder.find_dates(content, index=True)
        for match in matches:
            index = match[1]
            if index[1] - index[0] >= 10:
                center_format = '{:^' + str(index[1] - index[0]) + '}'
                replacement = center_format.format(match[0].strftime('%Y%m%d'))
                content = content[0: index[0]] + replacement + content[index[1]:]
        
        page.content = content
        return page
    
    def sig_handler(self, signum, frame):
        print('erro')
        self.index += 1
        self.handle()
    
    def handle(self, *args, **options):
        path = settings.BASE_DIR + '/politicians/'
        files = [file for file in listdir(path) if isfile(join(path, file))]
        
        for i, file in enumerate(files[self.index:]):
            self.stdout.write(str(i))
            self.index += 1
            page = self.extract(path, file)
            try:
                page.save()
            except:
                pass