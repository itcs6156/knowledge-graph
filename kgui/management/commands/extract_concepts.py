from django.core.management.base import BaseCommand

from kgui.models import Politician
from lib.extract_concepts import ExtractConcepts


class Command(BaseCommand):
    help = 'Extract the concepts'
    
    def add_arguments(self, parser):
        parser.add_argument('politician_id', nargs='+', type=int)
    
    def handle(self, *args, **options):
        politician_id = options['politician_id']
        politician = Politician.objects.all().filter(id=int(politician_id[0]))
        extract_concepts = ExtractConcepts()
        politician = politician[0]
        print(politician.content)
        concepts = extract_concepts.extract(politician.content)
        #print(concepts)
