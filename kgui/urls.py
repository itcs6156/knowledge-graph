from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:politician_id>/', views.detail, name='detail')
]
