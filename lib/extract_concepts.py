import math
import string
from html import unescape

import nltk
import regex
from nltk.corpus import wordnet as wn


class ExtractConcepts:
    def __init__(self):
        self.tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        self.stop_words = set(nltk.corpus.stopwords.words('english'))
        self.translate_punct = str.maketrans('', '', string.punctuation)
        self.regex_punctuations = regex.compile(r'[^\w\s]')
        
        grammar = r'NP: {<DT>?<JJ.*>*<NN.*>*}'
        self.chunker = nltk.RegexpParser(grammar, loop=2)
    
    def split_sentences(self, text):
        # Remove all new lines
        text = text.replace('\n', ' ')
        
        # Tokenize into sentences
        sentences = self.tokenizer.tokenize(text)
        
        # Remove punctuations except words
        sentences = [s.translate(self.translate_punct) for s in sentences]
        
        # Strip sentences
        sentences = [s.strip() for s in sentences]
        
        return sentences
    
    def clean_content(self, content):
        # Remove URLs
        content = regex.sub(r'https?:.*?\s', '', content)
        
        # Remove remaining tags
        content = regex.sub(r'\{\{.*?\}\}', '', content)
        
        # Remove HTML entities
        content = unescape(content)
        return content
    
    def is_relevant_np(self, phrase):
        phrase = phrase.flatten()
        words = [word[0].lower() for word in phrase]
        score = math.inf
        for i in range(len(words)):
            sub_phrase = '_'.join(words[i:])
            score = len(wn.synsets(sub_phrase))
            if score > 0:
                score = score / (i + 1)
                
        return score > 0 and score < 4
    
    def select_relevant_np(self, nodes):
        relevant = []
        
        for i, child in enumerate(nodes):
            if not isinstance(child, nltk.tree.Tree):
                continue
            
            if child.label() == 'NP' and self.is_relevant_np(child):
                relevant.append(child.leaves())
            elif child.label() != 'NP':
                relevant += self.select_relevant_np(child)
        return relevant
    
    def remove_stopwords(self, nodes):
        toDelete = []
        for i, child in enumerate(nodes):
            if isinstance(child, nltk.tree.Tree):
                self.remove_stopwords(child)
            elif child[0].lower() in self.stop_words:
                toDelete.append(i)
        
        if len(toDelete) > 0:
            for i in sorted(toDelete, reverse=True):
                del nodes[i]
        
        return nodes
    
    def print_concepts(self, concepts):
        for concept in concepts:
            concept = [c[0] for c in concept]
            print(' '.join(concept))
            
    def extract(self, content):
        content = self.clean_content(content)
        sentences = self.split_sentences(content)
        output_concepts = []
        
        for s in sentences:
            # Generate words
            words = nltk.tokenize.word_tokenize(s)
            
            # Perform POS Tagging
            words_tags = nltk.pos_tag(words)
            
            # Create Chunks
            words_chunks = self.chunker.parse(words_tags)
            
            # Remove Stopwords from the Chunks
            words_chunks = self.remove_stopwords(words_chunks)
            
            # Select relevant phrases
            relevant_concepts = self.select_relevant_np(words_chunks)
            output_concepts += relevant_concepts
            
        #self.print_concepts(output_concepts)
        
        return output_concepts

content = """
Abner Joseph Mikva (born     19260121     –  20160704   ) was an American politician, federal judge, lawyer and law professor. He was a member of the Democratic Party. Born in Milwaukee, Wisconsin, he spent his entire political and law career in his hometown in Chicago, Illinois.

Mikva served in the United States House of Representatives representing Illinois's 2nd congressional district (1969–1973) and 10th congressional district (1975–1979). He was appointed to the United States Court of Appeals for the District of Columbia Circuit by President Jimmy Carter, serving from 1979 through 1994. He served as the White House Counsel from 1994 through 1995 under the Clinton presidency.

During his later career, Mikva taught law at University of Chicago Law School and at Northwestern University. He mentored future President of the United States Barack Obama during his early years in law. In 2014, Obama honored Mikva by presenting him the Presidential Medal of Freedom.

Mikva died in Chicago at the age of 90 after suffering from bladder cancer.

Early life and family
Mikva was born in Milwaukee, Wisconsin, the son of Ida (Fishman) and Henry Abraham Mikva, Jewish immigrants from Ukraine.Mikva and his parents spoke Yiddish at home. During the Great Depression, his father was often unemployed and the family relied on welfare.  Abner attended local public schools. During World War II, he enlisted and was trained in the Army Air Corps, but the war ended the day before he was due to be deployed. Afterwards, the GI Bill enabled Mikva to attend the University of Wisconsin, Milwaukee before transferring to Washington University in St. Louis, Missouri, where he met his future wife, Zorita Rose (Zoe) Wise. Both graduated in 1948 and soon married.

The couple moved to Chicago, Illinois, where (as Zoe had urged) Mikva enrolled in law school at the University of Chicago. He received his J.D. in 1951. The couple eventually had three daughters: Mary Lane (b. 1963), an Illinois Appellate Court judge in Chicago;http://www.illinoiscourts.gov/appellatecourt/judges/Bio_Mikva.asp Laurie, who teaches at Northwestern University and is on the board of directors of the Legal Services Corporation; and Rachel, a rabbi and professor who teaches at the Chicago Theological Seminary.

Political career
After graduation, Mikva clerked for Supreme Court Justice Sherman Minton. He also returned to Chicago and began practicing law, at a firm which became Goldberg, Devoe, Shadur & Mikva after he made partner. The firm handles labor, real estate, commercial and civil rights cases, as well as some criminal defense.

Nonetheless, his early interest in Chicago clearly was politics:
One of the stories that is told about my start in politics is that on the way home from law school one night in 1948, I stopped by the ward headquarters in the ward where I lived. There was a street-front, and the name Timothy O'Sullivan, Ward Committeeman, was painted on the front window. I walked in and I said "I'd like to volunteer to work for [Adlai Stevenson] and [Paul Douglas]]." This quintessential Chicago ward committeeman took the cigar out of his mouth and glared at me and said, "Who sent you?" I said, "Nobody sent me." He put the cigar back in his mouth and he said, "We don't want nobody that nobody sent." This was the beginning of my political career in Chicago.Abner Mikva Interview: Conversations with History; Institute of International Studies, UC Berkeley    19990412    .

He spent ten years in the Illinois House of Representatives before serving in the U.S. Congress from 1969 to 1973 and 1975 to 1979. While in the House of Representatives, Mikva was part of the Kosher Nostra, a group of independent, clean Democrats that included future U.S. Senator and Presidential candidate Paul Simon, future Illinois Comptroller and candidate for Governor Dawn Clark Netsch, and Rep. Anthony Scariano.

He first represented Illinois' 2nd District, which included the South Side's lakefront wards including Hyde Park, his residence and also home to the University of Chicago. Both parties attempted to redistrict Mikva out of Congress. It led to the redistricting for the 1972 elections put Hyde Park in the 1st District. It was for the first time since 1903. It would have pitted Mikva against Democratic incumbent Ralph Metcalfe in a district with nearly a 90% black population; moving to stay in the 2nd District would have matched him against Democratic incumbent Morgan F. Murphy, who had previously represented the 3rd District. Mikva instead moved to the North Shore's 10th District.

After he was defeated by Republican Samuel H. Young in 1972, he successfully ran in 1974 Democratic wave election; his status was enhanced in the predominantly Republican, suburban district because he was viewed as critical of the Chicago Democratic establishment. In 1976, he was narrowly re-elected by 201 votes against Republican John Edward Porter in what was one of the most expensive congressional races to that time. When he won by a little over 2,000 votes in 1978, he joked to supporters that he had "won by a landslide."

Judicial career
]]     19790529     President Jimmy Carter nominated Mikva to the D.C. Circuit Court of Appeals. Despite opposition from anti-gun control interests that spent over $1 million to oppose his nomination, Mikva was confirmed by a 58–31 vote of the United States Senate       19790925       ."Dissenting Opinion", ''University of Chicago Magazine''  19960831   . Mikva then resigned his congressional seat (Porter succeeded Mikva after a special election).

Judge Mikva served on the D.C. Circuit from 1979 until his resignation in 1994 (two years before he would have been forced to step down as Chief Judge) to become White House Counsel to President Bill Clinton. During his 15 years as judge and four as Chief Judge, Mikva used his experience in the legislative branch as well as with the conservative Justice Minton, to craft his opinions. Mikva's most controversial decisions struck down the Pentagon ban against gays serving in the U.S. military (overturned on appeal by the circuit sitting en banc, but the ban was ultimately overturned by Executive Order), and in 1982 upholding regulation of air bags in automobiles.

In 1992, while serving as Chief Judge on the D.C. Circuit, Mikva appeared in the Kevin Kline comedy ''Dave'' as "Supreme Court Justice Abner J. Mikva," in a scene in which he administers the presidential oath of office to the Vice President (played by Ben Kingsley).

Post-judicial career
Mikva taught law at Northwestern University and was White House Counsel under President Bill Clinton from 1994 to 1995, finding himself the oldest member of the White House team, and eventually resigning due to exhaustion.

He then returned to the University of Chicago Law School, serving as the Schwarz Lecturer and the senior director of the Mandel Legal Aid Clinic. While at the University, Mikva came to better know future president Barack Obama, whom he mentored and supported politically. Obama awarded Mikva the Presidential Medal of Freedom      20141124       . Mikva had offered Obama a law clerk position in his judicial office after Obama graduated from Harvard Law School, but Obama declined. Future Obama appointee and U.S. Supreme Court Justice Elena Kagan did serve as one of Mikva's law clerks and was then a professor at the University of Chicago Law School. Mikva also encouraged Obama to listen to preachers to understand public speaking, "listen[ing] to patterns of speech, how to take people up the ladders. It's almost a Baptist tradition to make someone faint, and, by God, he's doing it now."

Mikva died under hospice care in Chicago, Illinois from complications of bladder cancer     20160704     aged 90. He was also suffering from chronic obstructive pulmonary disease at the time of his death.

Other pursuits
Mikva served as a mediator through JAMS, and was co-chairman of the Constitution Project's bipartisan Constitutional Amendments Committee.

In    20041130    Mikva was an international election monitor of Ukraine's contested presidential election. In  20060731  Illinois Governor Rod Blagojevich named Mikva chair of the Illinois Human Rights Commission.

In 2009, Illinois Governor Pat Quinn requested that Mikva lead a commission investigating the University of Illinois at Urbana–Champaign for admitting applicants (many of whom were not very well qualified) whose relatives or backers had connections to and had donated money to Illinois state lawmakers.

=Mikva Challenge=
Mikva and his wife Zoe started the Mikva Challenge, a civic leadership program for Chicago youth in 1997. The organization now has chapters in Washington, D.C. and Los Angeles. It helps youth to expand their political desire by working as election judges, volunteering on campaigns, advising city officials, and creating local activism projects to improve their schools and communities.

Legacy and awards
Mikva's congressional and judicial papers are archived at the Abraham Lincoln Presidential Library in Springfield, Illinois.Finding aid, Federal Judicial Center, Adam Mikva , FJC.gov

In 1998, Mikva received the Chicago History Museum's "Making History Award" for Distinction in Public Service.

In 2016, Congress renamed the U.S. post office in downtown Evanston, Illinois after Mikva, who had represented Evanston as a congressional representative.

See also

* List of Jewish members of the United States Congress

References


External links

*Mikva Challenge
*  Retrieved   20090226    *
*
*






|-



|-




|-

{{s-ttl|title=|years=1991–1994}}

|-
"""

c = ExtractConcepts()
c.extract(content)