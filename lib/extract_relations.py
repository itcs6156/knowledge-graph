import requests
import json
from urllib.parse import quote_plus


class ExtractRelations:
    @staticmethod
    def extract_relations(content):
        properties = dict(annotators='openie', outputFormat='json')
        url = 'http://localhost:9000/?properties=' + quote_plus(json.dumps(properties))
        response = requests.post(url, data=content.encode('utf-8'), headers={'Content-type': 'text/plain; charset=utf-8'})
        output = json.loads(response.text)
        return output
