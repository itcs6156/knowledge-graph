from os import listdir
from os.path import isfile, join

import datefinder
import regex

from models.page import Page
from mongoengine.errors import NotUniqueError


class LabelConcepts:
    def label(self, path, file):
        content = open(join(path, file), 'r').read()
        
        page = Page()
        
        # Extract Name
        match = regex.search(r'\'\'\'(.*?)\'\'\'', content)
        if match is not None:
            page.name = match.group(1).strip()
        else:
            page.name = file.replace('_', ' ')
        
        # Extract image file name
        match = regex.search(r'[=|:](.*\.jpg)', content)
        if match is not None:
            page.image = match.group(1).strip()
        
        # Extract URL references
        match = regex.findall(r'(https?://[^\s\]\}<]+)', content)
        if match is not None:
            page.references = set(match)
        
        # Remove infobox
        content = regex.sub(r'{((?>[^{}]+|(?R))*)}', '', content, regex.MULTILINE)
        
        # Remove other braces {{...}}
        content = regex.sub(r'{{[^{}]+?}}', '', content)
        
        # Remove all references <ref..>
        content = regex.sub(r'<[^>]+?>', '', content)
        
        # Remove all categories [[Category:...]]
        content = regex.sub(r'\[\[Category[^]]+\]\]?', '', content)
        
        # Remove inline images
        content = regex.sub(r'\[\[File[^\]]+\]\]?', '', content)
        
        # Remove square brackets with pipes and pick second option e.g. [[a|b]] replaces with b
        content = regex.sub(r'\[\[([^\|\[]+)\|([^\]]+)\]\]?', r'\2', content)
        
        # Remove remaining square brackets [[]]
        content = regex.sub(r'\[\[([^\]]+)\]\]?', r'\1', content)
        
        # Remove links and text in the square bracket
        content = regex.sub(r'\[http[^\s]+ ([^\]]+)]?', r'\1', content)
        
        # Indicate born date
        content = content.replace('\'\'\' (', '\'\'\' (born ')
        
        # Remove ''' from name
        content = content.replace("'''", '')
        
        # Remove == from section title
        content = content.replace('==', '')
        
        # Remove white spaces
        content = content.strip()
        
        # Convert date to YYYYMMDD formats
        matches = datefinder.find_dates(content, index=True)
        for match in matches:
            index = match[1]
            if index[1] - index[0] >= 10:
                center_format = '{:^' + str(index[1] - index[0]) + '}'
                replacement = center_format.format(match[0].strftime('%Y%m%d'))
                content = content[0: index[0]] + replacement + content[index[1]:]
        
        page.content = content
        return page
    
    def execute(self):
        path = '../politicians/'
        files = [file for file in listdir(path) if isfile(join(path, file))]
        
        for i, file in enumerate(files):
            try:
                page = self.extract(path, file)
                page.save()
            except NotUniqueError:
                pass


if __name__ == '__main__':
    extract_content = ExtractContent()
    extract_content.execute()
